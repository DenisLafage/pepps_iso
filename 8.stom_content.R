# Libraries

library(tidyverse)
library(vegan)
library(MANOVA.RM)
library(fmsb)

# D. labrax

## Anosim

stc_lab <- stom_cont_eff %>%
  filter(sp == 'Bar') %>%
  mutate(s = rowSums(.[2:18])) %>% 
  filter(s !=0) %>% 
  select(-c(s, ostracodes, Cicadomorpha, Larve_poisson, Gasteropode,
            Œufs, Acarien))


labrax_dist <- vegdist(
  stc_lab %>% 
    select(-c(sp, site, period))
)
  

anosim(labrax_dist, stc_lab$site)
anosim(labrax_dist, stc_lab$period)

## PERMANOVA

stc_lab2 <- stc_lab %>% 
  mutate(loc = substr(site,1,1),
         rest = case_when(site %in% c('SN', 'CA', 'M','PN')~ 'Natural',
                          TRUE ~ 'Restored')) %>% 
  filter(site !='M')

adonis2(stc_lab %>% 
          select(-c(sp, site, period)) ~ site + period, data = stc_lab)

adonis2(stc_lab %>% 
          select(-c(sp, site, period)) ~  period, contr.unordered = rest, strata = loc, data = stc_lab2)

# # RM Manova
# 
# rmm_labrax <- multRM(cbind(amphipodes, larve_diptere,Polychetes,
#                            Palaemonidae, diptere_brachycere,
#                            Mysidacea, sphaeroma, Crangon_crangon,
#                            crabe, Cumacea, Corixidae
#                            ) ~ 1, data = stc_lab2, subject = "loc", within = "rest", 
#                      iter = 200, CPU = 1)
# summary(fit)

## Radar plot

### Prep data

mfi_lab_C <- mfi %>% 
  filter(species == 'Bar') %>%
  filter(site %in% c('CA', 'CR')) %>% 
  select(-c(Ostracoda, Cicadomorpha, `Fish larvae`, Gasteropoda, Eggs, Acari))

mfi_lab_C.df <- as.data.frame(mfi_lab_C[,-c(1,2)])
rownames(mfi_lab_C.df) <- mfi_lab_C$site


mfi_lab_S <- mfi %>% 
  filter(species == 'Bar') %>%
  filter(site %in% c('SN', 'SD')) %>% 
  select(-c(Ostracoda, Cicadomorpha, `Fish larvae`, Gasteropoda, Eggs, Acari))

mfi_lab_S.df <- as.data.frame(mfi_lab_S[,-c(1,2)])
rownames(mfi_lab_S.df) <- mfi_lab_S$site

mfi_lab_M <- mfi %>% 
  filter(species == 'Bar') %>%
  filter(site %in% 'M') %>% 
  select(-c(Ostracoda, Cicadomorpha, `Fish larvae`, Gasteropoda, Eggs, Acari))

mfi_lab_M.df <- as.data.frame(mfi_lab_M[,-c(1,2)])
rownames(mfi_lab_M.df) <- mfi_lab_M$site

par(family = "Fira Sans",mfrow = c(1,2))

#### CROZON

mfi_lab_C.df=rbind(rep(40,11) , rep(0,11) , mfi_lab_C.df) #add extrem values

colors_border_AF=c( rgb(0.2,0.5,0.5,0.9), rgb(0.8,0.2,0.5,0.9))
colors_in_AF=c(rgb(0.2,0.5,0.5,0.4), rgb(0.8,0.2,0.5,0.4))
radarchart( mfi_lab_C.df  , axistype=1 , 
            #custom polygon
            pcol=colors_border_AF , pfcol=colors_in_AF , plwd=4 , plty=1,
            #custom the grid
            cglcol="grey", cglty=1, axislabcol="grey", caxislabels=seq(0,40,10), cglwd=0.8,
            #custom labels
            vlcex=0.8 
)
legend(x=0.7, y=1, legend = rownames(mfi_lab_C.df[-c(1,2),]), bty = "n", pch=20 , col=colors_in_AF , text.col = "black", cex=1.2, pt.cex=3)

### SENE


mfi_lab_S.df=rbind(rep(40,11) , rep(0,11) , mfi_lab_S.df) # add extrem values


colors_border_AF=c( rgb(0.2,0.5,0.5,0.9), rgb(0.8,0.2,0.5,0.9))
colors_in_AF=c(rgb(0.2,0.5,0.5,0.4), rgb(0.8,0.2,0.5,0.4))
radarchart( mfi_lab_S.df  , axistype=1 , 
            #custom polygon
            pcol=colors_border_AF , pfcol=colors_in_AF , plwd=4 , plty=1,
            #custom the grid
            cglcol="grey", cglty=1, axislabcol="grey", caxislabels=seq(0,40,10), cglwd=0.8,
            #custom labels
            vlcex=0.8 
)
legend(x=0.7, y=1, legend = rownames(mfi_lab_S.df[-c(1,2),]), bty = "n", pch=20 , col=colors_in_AF , text.col = "black", cex=1.2, pt.cex=3)

### MESQUER -> only 2 prey





# P. microps

## Anosim

stc_mic <- stom_cont_eff %>%
  filter(sp == 'Gobie') %>%
  mutate(s = rowSums(.[2:18])) %>% 
  filter(s !=0) %>%
  filter(site !='M') %>% 
  select(-c(s, Cicadomorpha, Palaemonidae, diptere_brachycere,
            Gasteropode, Crangon_crangon, crabe, Corixidae))


microps_dist <- vegdist(
  stc_mic %>% 
    select(-c(sp, site, period))
)


anosim(microps_dist, stc_mic$site)
anosim(microps_dist, stc_mic$period)

## PERMANOVA

stc_mic2 <- stc_mic %>% 
  mutate(loc = substr(site,1,1),
         rest = case_when(site %in% c('SN', 'CA', 'M','PN')~ 'Natural',
                          TRUE ~ 'Restored')) 

adonis2(stc_mic2 %>% 
          select(-c(sp, site, period, rest, loc)) ~ site + period, data = stc_mic2)

adonis2(stc_mic2 %>% 
          select(-c(sp, site, period, rest, loc)) ~ period + rest + (1|loc), data = stc_lab2)

# RM Manova

rmm_mic <- multRM(cbind(amphipodes, ostracodes,larve_diptere,Polychetes,
                        Mysidacea, Larve_poisson,sphaeroma, Cumacea,Œufs,Acarien
) ~ 1, data = stc_mic2, subject = "loc", within = "rest", 
iter = 200, CPU = 1)
summary(fit)


## Radar plot

### Prep data

mfi_mic_P <- mfi %>% 
  filter(species == 'Gobie') %>%
  filter(site %in% c('PN', 'PD')) %>% 
  select(-c(Cicadomorpha, Palaemonidae, Diptera, Gasteropoda, Crangonidae, Carcinidae, Corixidae))

mfi_mic_P.df <- as.data.frame(mfi_mic_P[,-c(1,2)])
rownames(mfi_mic_P.df) <- mfi_mic_P$site


mfi_mic_S <- mfi %>% 
  filter(species == 'Gobie') %>%
  filter(site %in% c('SN', 'SD')) %>% 
  select(-c(Cicadomorpha, Palaemonidae, Diptera, Gasteropoda, Crangonidae, Carcinidae, Corixidae))

mfi_mic_S.df <- as.data.frame(mfi_mic_S[,-c(1,2)])
rownames(mfi_mic_S.df) <- mfi_mic_S$site



par(family = "Fira Sans",mfrow = c(1,2))
par(mfrow =c(1,1))
#### PLURIEN

mfi_mic_P.df=rbind(rep(60,10) , rep(0,10) , mfi_mic_P.df) #add extrem values

colors_border_AF=c( rgb(0.2,0.5,0.5,0.9), rgb(0.8,0.2,0.5,0.9))
colors_in_AF=c(rgb(0.2,0.5,0.5,0.4), rgb(0.8,0.2,0.5,0.4))
radarchart( mfi_mic_P.df  , axistype=1 , 
            #custom polygon
            pcol=colors_border_AF , pfcol=colors_in_AF , plwd=4 , plty=1,
            #custom the grid
            cglcol="grey", cglty=1, axislabcol="grey", caxislabels=seq(0,60,15),
            cglwd=0.8,
            #custom labels
            vlcex=0.8 
)
legend(x=0.7, y=1, legend = rownames(mfi_mic_P.df[-c(1,2),]), bty = "n", pch=20 , col=colors_in_AF , text.col = "black", cex=1.2, pt.cex=3)

### SENE


mfi_mic_S.df=rbind(rep(60,10) , rep(0,10) , mfi_mic_S.df) # add extrem values


colors_border_AF=c( rgb(0.2,0.5,0.5,0.9), rgb(0.8,0.2,0.5,0.9))
colors_in_AF=c(rgb(0.2,0.5,0.5,0.4), rgb(0.8,0.2,0.5,0.4))
radarchart( mfi_mic_S.df  , axistype=1 , 
            #custom polygon
            pcol=colors_border_AF , pfcol=colors_in_AF , plwd=4 , plty=1,
            #custom the grid
            cglcol="grey", cglty=1, axislabcol="grey", caxislabels=c(0,15,30,45,60), cglwd=0.8,
            #custom labels
            vlcex=0.8 
)
legend(x=0.7, y=1, legend = rownames(mfi_mic_S.df[-c(1,2),]), bty = "n", pch=20 , col=colors_in_AF , text.col = "black", cex=1.2, pt.cex=3)

### MESQUER -> only 2 prey


## Vacuite

p_vac <- ggplot(data = vac %>% filter(Site !="Mesquer") %>% filter(sp %in% c('Bar', 'Gobie')),
                aes(x= Site, y = Vacuite))

p_vac + geom_col(position = "dodge2", aes(fill = sp)) + facet_grid(rows = vars(Date))
